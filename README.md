<p align="center">
    <img src="https://raw.githubusercontent.com/MahdyMirzade/MahdyMirzade/main/assets/dumbash/dumbash_main.png">
</p>
<p align="center">
    :iran::keyboard: Replace your second keyboard layout characters to English in <b>bash</b> and <b>zsh</b>. (Persian)
</p>

## Installation
1. Download `dumBash.sh`:
```
$ git clone https://github.com/MahdyMirzade/dumBash.git ~/dumBash
$ cd ~/dumBash
$ bash setup.sh
```
2. Restart terminal or shell.

## Configuration
Run `config.sh` script:
```
$ cd ~/dumBash
$ bash config.sh
```
![dumBash Configuration](https://raw.githubusercontent.com/MahdyMirzade/MahdyMirzade/main/assets/dumbash/dumbash_config.png)

## Team
This project is maintained by the following people and a bunch of [awesome contributors](https://github.com/MahdyMirzade/dumBash/graphs/contributors).
| [![Mahdy Mirzade](https://github.com/mahdymirzade.png?size=100)](https://github.com/mahdymirzade) | [![Poorya Asadi](https://github.com/pr-asadi.png?size=100)](https://github.com/pr-asadi)  |
| --- | --- |
| [Mahdy Mirzade](https://github.com/mahdymirzade) | [Poorya Asadi](https://github.com/pr-asadi) |
